#!/bin/bash

# installation 
sudo apt-get install software-properties-common -y -qq
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update
sudo apt-get install ansible -y -qq

sudo DEBIAN_FRONTEND=noninteractive apt-get install python-dev libkrb5-dev krb5-user -y -q
sudo apt-get install python-pip -y -qq
sudo pip install kerberos requests_kerberos
sudo pip install pywinrm==0.1.1

# TODO: modify the /etc/krb5.conf file
sudo sed -i 's/.*\[realms\].*/&\n\tINTELEX.COM = {\n\t\tkdc = tor01dc03.intelex.com\n\t}/' /etc/krb5.conf
sudo sed -i 's/.*\[domain_realm\].*/&\n\t\.intelex.com = INTELEX\.COM/' /etc/krb5.conf
